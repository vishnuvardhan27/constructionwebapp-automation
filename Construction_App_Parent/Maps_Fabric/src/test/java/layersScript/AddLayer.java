package layersScript;

import org.testng.annotations.Test;

import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;
import log4j2_Implementation.log4j_main;
import sidePannel_POM.Layers_Common_Objects;

public class AddLayer  extends LoginSetup{
	@Test(groups = {"Regression"})
	public void addLayer() throws Exception {
		Searchentitiesobjects searchentitiesobjects=new Searchentitiesobjects(driver);
		Layers_Common_Objects layers_Common_Objects=new Layers_Common_Objects(driver);
		log4j_main.init();
		log4j_main.startTestCase("Adding Layer.");
		searchentitiesobjects.SearchEntity_Map(Constant.addDataToMap);
		layers_Common_Objects.clickOnRightArrowButton();
		layers_Common_Objects.clickOn_Layers();
		layers_Common_Objects.clickOn_AddData();
		layers_Common_Objects.clickOnLoadFiles();
		layers_Common_Objects.fileUpload();
		layers_Common_Objects.verify_FileUploaded();
		layers_Common_Objects.clickOnAddLayerPlusButton();
		log4j_main.endTestCase("Adding Layer.");
	}
}
