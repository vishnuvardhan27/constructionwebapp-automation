package layersScript;

import org.testng.annotations.Test;

import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;
import sidePannel_POM.Layers_Common_Objects;

public class AddDataToMap extends LoginSetup{
	@Test(groups = {"Regression"})
	public void addDataToMap() throws Exception {
		Searchentitiesobjects searchentitiesobjects=new Searchentitiesobjects(driver);
		Layers_Common_Objects layers_Common_Objects=new Layers_Common_Objects(driver);
		
		searchentitiesobjects.SearchEntity_Map(Constant.addDataToMap);
		layers_Common_Objects.clickOnRightArrowButton();
		layers_Common_Objects.clickOn_Layers();
		layers_Common_Objects.clickOn_AddData();
		layers_Common_Objects.clickOnLoadFiles();
		layers_Common_Objects.fileUpload();
		layers_Common_Objects.verify_FileUploaded();
		layers_Common_Objects.click_ShowDataTable();
		layers_Common_Objects.delete_DataSet();
		layers_Common_Objects.verify_FileDeleted();
		layers_Common_Objects.click_LayerBlending();
		
	}
}
