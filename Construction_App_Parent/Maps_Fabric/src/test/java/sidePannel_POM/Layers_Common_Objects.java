package sidePannel_POM;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Layers_Common_Objects extends BaseDriver{

	public Layers_Common_Objects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	
	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 60);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	JavascriptExecutor je = (JavascriptExecutor) driver;
	
	
	By rightArrowButton=By.xpath("//*[contains(@class,'side-panel--container')]//*[contains(@class,'arrowright')]");
	By layersNavButton=By.xpath("//*[@data-for='layer-nav']//*[contains(@class,'layers')]");
	By layersText=By.xpath("//*[contains(@class,'title') and text()='Layers']");
	By addDataButton=By.xpath("//*[contains(@class,'add-data-button')]");
	By layerBlendingDropdown=By.xpath("//*[contains(@class,'item-selector__dropdown')]//*[contains(@class,'dropdown__value')]");
	By addDataToMapText=By.xpath("//*[text()='Add Data To Map']");
	By addDataToMapPopup=By.xpath("//*[contains(@class,'ReactModalPortal')]//*[@role='dialog']");
	By loadFiles=By.xpath("//*[contains(@class,'load-data-modal__tab__inner')]//*[text()='Load Files']");
	By uploadFileMessage=By.xpath("//*[contains(@class,'file-upload__message')]");
	By browseYourFiles=By.xpath("//*[text()='browse your files']");
	By uploadedFile=By.xpath("//*[contains(@class,'source-data-catalog')]//*[contains(@class,'source-data-title')]");
	By loadVisurDataset=By.xpath("//*[contains(@class,'load-data-modal__tab__inner')]//*[text()='Load Visur Dataset']");
	By loadPrivateFiles=By.xpath("//*[contains(@class,'load-data-modal__tab__inner')]//*[text()='Load Private Files']");
	By loadPublicFiles=By.xpath("//*[contains(@class,'load-data-modal__tab__inner')]//*[text()='Load Public Files']");
	By showDataTable=By.xpath("//*[contains(@class,'source-data-catalog')]//*[@class='data-ex-icons-table ']");
	By removeDataset=By.xpath("//*[contains(@class,'source-data-catalog')]//*[@class='data-ex-icons-trash ']");
	By deleteDatasetPopup=By.xpath("//*[@role='dialog']//*[contains(@class,'modal--wrapper')]");
	By close_datasetPopup=By.xpath("//*[@role='dialog']//*[contains(@class,'modal--wrapper')]//*[contains(@class,'modal--close')]");
	By layerBlending=By.xpath("//*[text()='Layer Blending']/..//*[contains(@class,'active item-selector__dropdown')]");
	By addLayer_Button=By.xpath("//*[contains(@class,'add-layer-button')]");
	By newLayerField=By.xpath("//*[@type='text' and @value='new layer']");
	
	
	public void clickOnRightArrowButton() {
		wdc.waitForPageLoad();
		boolean obj=true;
		int i=0;
		while(obj) {
			try {
				if(i<3) {
					WebElement arrowButton=driver.findElement(rightArrowButton);
					wdc.waitForExpactedElement(arrowButton);
					actions.click(arrowButton).build().perform();
					wdc.waitForPageLoad();
					System.out.println("Right Arrow Button Clicked.");
					if (driver.findElement(By.xpath("//*[contains(@class, 'side-panel--container')]")).isDisplayed()) {
						System.out.println("Side Panel Container Displayed.");
						obj=false;
					}
				}
				else {
					obj= false;
					System.out.println("Side Panel Container not Displayed.");
				}
			}
			catch (Exception e) {
				i++;
			}
		}
	}
	
	public void clickOn_Layers() {
		wdc.waitForPageLoad();
		boolean obj=true;
		int i=0;
		while(obj) {
			try {
				if(i<3) { 
					WebElement layersButton=driver.findElement(layersNavButton);
					wdc.waitForExpactedElement(layersButton);
					actions.click(layersButton).build().perform();
					wdc.waitForPageLoad();
					System.out.println("Layers Nav Button Clicked.");
					if (driver.findElement(layersText).isDisplayed()) {
						System.out.println("Layers container displayed.");
						obj=false;
					}
				}
				else {
					obj= false;
					System.out.println("Layers Nav Button not Clicked.");
				}
			}
			catch (Exception e) {
				i++;
			}
		}
	}
	
	public void clickOn_AddData() {
		wdc.waitForPageLoad();
		boolean obj=true;
		int i=0;
		while(obj) {
			try {
				if(i<3) { 
					WebElement addButton=driver.findElement(addDataButton);
					wdc.waitForExpactedElement(addButton);
					actions.click(addButton).build().perform();
					wdc.waitForPageLoad();
					System.out.println("Add Button Clicked.");
					if (driver.findElement(addDataToMapPopup).isDisplayed()
							|| driver.findElement(addDataToMapText).isDisplayed()) {
						System.out.println("Add Data To Map Popup displayed.");
						obj=false;
					}
				}
				else {
					obj= false;
					System.out.println("Add Data To Map Popup not displayed.");
				}
			}
			catch (Exception e) {
				i++;
			}
		}
	}
	
	public void clickOnLoadFiles() {
		wdc.waitForPageLoad();
		boolean obj=true;
		int i=0;
		while(obj) {
			try {
				if(i<3) { 
					WebElement LoadFiles=driver.findElement(loadFiles);
					wdc.waitForExpactedElement(LoadFiles);
					actions.click(LoadFiles).build().perform();
					wdc.waitForPageLoad();
					System.out.println("LoadFiles Button Clicked.");
					if (driver.findElement(uploadFileMessage).isDisplayed()) {
						System.out.println("Drag & Drop your files here-- displayed.");
						obj=false;
						break;
					}
				}
				else {
					obj= false;
					System.out.println("Drag & Drop your files here-- not displayed.");
				}
			}
			catch (Exception e) {
				i++;
			}
		}
	}
	
	public void fileUpload () throws AWTException {
		wdc.waitForPageLoad();
		driver.findElement(browseYourFiles).click();
		wdc.waitForPageLoad();
        StringSelection strSelection = new StringSelection(getMap_FilePath());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(strSelection, null);
 
        Robot robot = new Robot();
  
        robot.delay(300);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(200);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }
	
	public String getMap_FilePath() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
			try {
				prop.load(fis);
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return prop.getProperty("MAP_File_Path");
	}
	
	public void verify_FileUploaded() {
		try {
			if (driver.findElement(uploadedFile).isDisplayed()) {
				System.out.println("Successfully uploaded files.");
			}
		} catch (Exception e) {
			System.out.println("Not uploaded files.");
		}
	}
	
	public void click_ShowDataTable() {
		try {
			wdc.waitForPageLoad();
			actions.moveToElement(driver.findElement(showDataTable)).build().perform();
			wdc.waitForPageLoad();
			if (driver.findElement(showDataTable).isDisplayed()) {
				System.out.println("Show Data Table button displayed.");
				driver.findElement(showDataTable).click();
				wdc.waitForPageLoad();
				if (driver.findElement(By.xpath("//*[@id='dataset-modal']")).isDisplayed()) {
					System.out.println("Map Data Set Table Popup displayed.");
					wdc.waitForPageLoad();
					driver.findElement(By.xpath("//*[contains(@class,'modal--close')]")).click();
					wdc.waitForPageLoad();
				}
			}
		} catch (Exception e) {
			System.out.println("Not clicked show data table.");
		}
	}
	
	public void delete_DataSet() {
		try {
			wdc.waitForPageLoad();
			actions.moveToElement(driver.findElement(removeDataset)).build().perform();
			wdc.waitForPageLoad();
			if (driver.findElement(removeDataset).isDisplayed()) {
				System.out.println("Remove Dataset button displayed.");
				driver.findElement(removeDataset).click();
				wdc.waitForPageLoad();
				if (driver.findElement(deleteDatasetPopup).isDisplayed()) {
					System.out.println("Delete Data Set Popup displayed.");
					wdc.waitForPageLoad();
					driver.findElement(By.xpath("//*[text()='Delete']")).click();
					wdc.waitForPageLoad();
				}
			}
		} catch (Exception e) {
			System.out.println("Not clicked Dataset button. ");
		}
	}
	
	public void verify_FileDeleted() {
		try {
			if (driver.findElement(uploadedFile).isDisplayed()) {
				System.out.println("Not deleted file.");
			}
		} catch (Exception e) {
			System.out.println("Successfully deleted file.");
		}
	}
	
	public void click_LayerBlending() {
		try {
			wdc.waitForPageLoad();
			driver.findElement(layerBlending).click();
			wdc.waitForPageLoad();
			List<WebElement> list=driver.findElements(By.xpath("//*[text()='Layer Blending']/..//*[contains(@class,'list__item__anchor')]"));
			for(WebElement e: list) {
				System.out.println(e);
			}
		} catch (Exception e) {
			System.out.println("Not cliked layer blending?");
		}
	}
	
	public void clickOnAddLayerPlusButton() {
		try {
			wdc.waitForPageLoad();
			if (driver.findElement(addLayer_Button).isDisplayed()) {
				System.out.println("Add Layer Plus Button displayed.");
				driver.findElement(addLayer_Button).click();
				wdc.waitForPageLoad();
				if (driver.findElement(newLayerField).isDisplayed()) {
					System.out.println("Successfully clicked on Add Layer Plus Button.");
				}
			}
		} catch (Exception e) {
			System.out.println("Add Layer Plus Button not displayed?");
		}
	}
	
	public void updateLayerName() {
		try {
			wdc.waitForPageLoad();
			driver.findElement(addLayer_Button).click();
			System.out.println("Clicked on Add Layer Plus Button.");
			if (driver.findElement(newLayerField).isDisplayed()) {
				System.out.println("Successfully clicked on Add Layer Plus Button.");
				
			}
		} catch (Exception e) {
			System.out.println("");
		}
	}
	
}
