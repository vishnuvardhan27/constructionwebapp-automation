package Construction_Admin_Script;
import org.testng.annotations.Test;
import Construction_Admin_Object.Construction_Admin_Object;
import Utils.Constant;
import Utils.LoginSetup;

public class ConstructionAdmin_Scripts extends LoginSetup{
	
	@Test
	public void constructionAdmin() throws Exception{
		System.out.println("ConstructionAdmin_Scripts----Execution Started");
		Construction_Admin_Object construction_Admin_Object = new Construction_Admin_Object(driver);
			
		construction_Admin_Object.selectProject(Constant.project);
		construction_Admin_Object.clickOnuserprofilebutton();
		construction_Admin_Object.clickOnConstructionAdmin();				
		construction_Admin_Object.clickOnAddButton();
	    construction_Admin_Object.clickOnFolderName(Constant.folderName);
		construction_Admin_Object.clickOnParentFolderName(Constant.parentfolderName);
		construction_Admin_Object.selectType(Constant.Type);
		construction_Admin_Object.clickOnSaveButton();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnApprovalsTab();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnTaggedItemsTab();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnPhysicalInstanceTab();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnMaterialsTab();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnDocumentsTab();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnDirectoryTab();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnProcedureTab();
		construction_Admin_Object.verifyCreatedTurnOver();
		
		construction_Admin_Object.clickOnTurnOverTab();
		construction_Admin_Object.verifyCreatedTurnOver();

	
		construction_Admin_Object.clickOnDeleteButton();
		construction_Admin_Object.verifyDeletedturnOver();
		 
		System.out.println("ConstructionAdmin_Scripts----Execution Successfull");		
	}

}
