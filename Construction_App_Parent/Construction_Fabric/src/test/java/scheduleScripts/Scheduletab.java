package scheduleScripts;

import org.testng.annotations.Test;

import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;
import schedulePOM.Schedule_obj;

public class Scheduletab extends LoginSetup{

		@Test
		public void schedule() throws Exception{
			
			Schedule_obj scheduletabobj = new Schedule_obj(driver);
			Searchentitiesobjects searchentitiesobjects=new Searchentitiesobjects(driver);
			
			searchentitiesobjects.searchAndclick_Construction_project(Constant.Projectname);
			scheduletabobj.Expand_Permitting();
			scheduletabobj.Expand_FID();
			scheduletabobj.Expand_FEEDProcess();
	}
}
