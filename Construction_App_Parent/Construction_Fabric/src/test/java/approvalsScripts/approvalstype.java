package approvalsScripts;
import org.testng.annotations.Test;

import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;
import approvalsPOM.approvalsCommonObjects;

public class approvalstype  extends LoginSetup{

	@Test
	public void ApprovalsTab() throws Exception{
		Searchentitiesobjects search = new Searchentitiesobjects(driver);
		approvalsCommonObjects approvalscommon = new approvalsCommonObjects(driver);
		
		approvalscommon.clickonapprovalstab();
		search.searchAndclick_Construction_project(Constant.Projectname);
		approvalscommon.clickapprovaltypefield();
		approvalscommon.selectapprovalstype(Constant.Journals);
		approvalscommon.clickexhibit();
//		approvalscommon.reviews_comments(Constant.reviewcommant);
//		approvalscommon.approval_comments(Constant.approvalscomment);
		approvalscommon.approvalsname(Constant.approvalsname);
		approvalscommon.reviewby(Constant.reviewby);
		approvalscommon.savebutton();
		approvalscommon.formclosebutton();
	}
}