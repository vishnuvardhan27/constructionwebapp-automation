package approvalsPOM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class approvalsCommonObjects extends BaseDriver{

	public approvalsCommonObjects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	Actions actions = new Actions(driver);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	
	By clickapprovalstypeoption = By.xpath("//*[@class='ng-select-container ng-has-value']");
	By clickexhibit = By.xpath("//*[@id='drop-block']//*[@id='dragtarget']");
	By approvals = By.xpath("//*[@class='nav-bar']//*[2][@mattooltipposition='right']");
	By commentbox = By.xpath("//*[@data-placeholder='Insert text here ...']");
	By approvalsname = By.xpath("//*[@data-placeholder='Approval Name']");
	By reviewbyname = By.xpath("//*[@placeholder='Review By']");
	By savebutton = By.xpath("//*[@class='head-route inactive-icon-opacity']");
	By closeform = By.xpath("//*[@id='construction-form-header']//*[@ng-reflect-message='CLOSE']");
	
	public void clickonapprovalstab() throws Exception{
		try {
			do {
				driver.findElement(approvals).click();
				System.out.println("Approvals Tab Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Approvals ']")).isDisplayed()) {
						System.out.println("Successfully Logged In... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (!driver.findElement(approvals).getAttribute("class").contains("active"));
		} catch (Exception e) {
			System.out.println("Approvals Error!!! !!! !!! !!!");
		}
	}
	
	public void clickapprovaltypefield() throws Exception{
		WebElement clickapprolvals = driver.findElement(clickapprovalstypeoption);
		wdc.waitForExpactedElement_150(clickapprolvals);
		actions.click(clickapprolvals).build().perform();
	}
	
	public void selectapprovalstype(String s) throws Exception{
		List<WebElement> selectapprovals = driver.findElements(By.xpath("//*[@role='option']"));
		wdc.waitForExpectedAllElements(selectapprovals);
				
		int j = 0;
		for (WebElement e : selectapprovals) {
			if (e.getText().equals(s)) {
				actions.click(e).build().perform();
				break;
			}
			j++;
		}		
	}
	
	public void clickexhibit() throws Exception{
		List<WebElement> clickjounrnal = driver.findElements(clickexhibit);
		wdc.waitForExpectedAllElements(clickjounrnal);
		clickjounrnal.get(0).click();
		System.out.println("clicked on journal");
	}
	
	public void reviews_comments(String s) throws Exception{
		List<WebElement> reviewcomment = driver.findElements(commentbox);
		wdc.waitForExpectedAllElements(reviewcomment);
		WebElement e = reviewcomment.get(0);
		actions.doubleClick(e).sendKeys(s).sendKeys(Keys.TAB).build().perform();
		System.out.println("entered the comments in review comment box");
	}
	
	public void approval_comments(String s) throws Exception{
		List<WebElement> approvalcomment = driver.findElements(commentbox);
		wdc.waitForExpectedAllElements(approvalcomment);		
		WebElement e = approvalcomment.get(1);
		actions.doubleClick(e).sendKeys(s).sendKeys(Keys.TAB).build().perform();
		System.out.println("entered the comments in approvals comment box");
	}
	
	public void approvalsname(String s) throws Exception{
		WebElement approvalname = driver.findElement(approvalsname);
		wdc.waitForExpactedElement_150(approvalname);
		actions.doubleClick(approvalname).sendKeys(s).sendKeys(Keys.TAB).build().perform();
	}
	
	public void reviewby(String s) throws Exception{
		WebElement reviewby = driver.findElement(reviewbyname);
		wdc.waitForExpactedElement_150(reviewby);
		actions.doubleClick(reviewby).sendKeys(s).sendKeys(Keys.TAB).build().perform();
	}
	
	public void savebutton() throws Exception{
		WebElement savebtn = driver.findElement(savebutton);
		wdc.waitForExpactedElement_150(savebtn);
		actions.click(savebtn).build().perform();
	}
	
	public void formclosebutton() throws Exception{
		WebElement closebtn = driver.findElement(closeform);
		wdc.waitForExpactedElement_150(closebtn);
		actions.click(closebtn).build().perform();
	}
	
}