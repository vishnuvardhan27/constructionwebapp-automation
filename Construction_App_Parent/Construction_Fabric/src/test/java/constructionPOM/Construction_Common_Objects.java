package constructionPOM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Construction_Common_Objects extends BaseDriver{

	public Construction_Common_Objects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	WebDriverCommonLib wdc=new WebDriverCommonLib(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	
	By approval_LeftTree= By.xpath("//*[@class='nav-bar']//*[2][@mattooltipposition='right']");
	By taggedItems_LeftTree= By.xpath("//*[@class='nav-bar']//*[3][@mattooltipposition='right']");
	By physicalInstances_LeftTree= By.xpath("//*[@class='nav-bar']//*[4][@mattooltipposition='right']");
	By materials_LeftTree= By.xpath("//*[@class='nav-bar']//*[5][@mattooltipposition='right']");
	By documents_LeftTree= By.xpath("//*[@class='nav-bar']//*[6][@mattooltipposition='right']");
	By directory_LeftTree= By.xpath("//*[@class='nav-bar']//*[7][@mattooltipposition='right']");
	By procedures_LeftTree= By.xpath("//*[@class='nav-bar']//*[8][@mattooltipposition='right']");
	By turnOver_LeftTree= By.xpath("//*[@class='nav-bar']//*[9][@mattooltipposition='right']");
	
	
	public void clickOnLeftTree_Approval() {
		try {
			do {
				driver.findElement(approval_LeftTree).click();
				System.out.println("approval Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Approvals ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on approval... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(approval_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("approval Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_taggedItems() {
		try {
			do {
				driver.findElement(taggedItems_LeftTree).click();
				System.out.println("Tagged Items Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Tagged Items ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Tagged Items... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(taggedItems_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("approval Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_physicalInstances() {
		try {
			do {
				driver.findElement(physicalInstances_LeftTree).click();
				System.out.println("Physical Instances Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Physical Instances ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Physical Instances... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(physicalInstances_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Physical Instances Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_materials() {
		try {
			do {
				driver.findElement(materials_LeftTree).click();
				System.out.println("Materials Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Materials ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Materials... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(materials_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Materials Error!!! !!! !!! !!!");
		}
	}
	
	
	public void clickOnLeftTree_documents() {
		try {
			do {
				driver.findElement(documents_LeftTree).click();
				System.out.println("Documents Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Documents ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Documents... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(documents_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Documents Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_Directory() {
		try {
			do {
				driver.findElement(directory_LeftTree).click();
				System.out.println("Directory Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Directory ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Directory... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(directory_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Directory Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_Procedures() {
		try {
			do {
				driver.findElement(procedures_LeftTree).click();
				System.out.println("Procedures Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Procedures ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Procedures... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(procedures_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Procedures Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_Turnover() {
		try {
			do {
				driver.findElement(turnOver_LeftTree).click();
				System.out.println("Turnover Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Turnover ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Turnover... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(turnOver_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Turnover Error!!! !!! !!! !!!");
		}
	}
}
