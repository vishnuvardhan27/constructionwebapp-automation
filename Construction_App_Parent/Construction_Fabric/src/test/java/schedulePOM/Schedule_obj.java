package schedulePOM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Schedule_obj extends BaseDriver{
        	  
	         public Schedule_obj(WebDriver driver) {
		     super(driver);
		     PageFactory.initElements(driver, this);
	         }
	         
		    WebDriverCommonLib wdc=new WebDriverCommonLib(driver);
		    WebDriverWait wait = new WebDriverWait(driver, 2);
		    Actions actions=new Actions(driver);
	        
	        public By Clickon_Schedule=By.xpath("//*[@class='nav-bar']//*[1][@mattooltipposition='right']");
	        public By Clickon_wbc=By.xpath("//*[@class='table-collapse-icon']//*[@class='i-icons fill']");
	        public By Expand_task=By.xpath("//*[@id='drop-block']//*[@id='dragtarget']");
	        public By From_Body=By.xpath("//*[@id='construction-form-body']");
	        public By Clickon_BackButton=By.xpath("//*[@class='head-route inactive-icon-opacity']//*[@data-mat-icon-name='GoBackArrow']");
	        
	        
	        public  void Clickon_Schedule() throws Exception {
		    	WebElement EntityType=driver.findElement(Clickon_Schedule);
		    	wdc.waitForExpactedElement(EntityType);
		   		actions.click(EntityType).build().perform();		
		   		System.out.println("clicked on Schedule");
		   		Thread.sleep(3000);
	        }
	        
	        public void Expand_Permitting() throws Exception {
	        	List<WebElement> list= driver.findElements(Expand_task);
	        	wdc.waitForPageLoad();
	        	
	        	try {
	        	if(list.get(0).isDisplayed()){
	        		list.get(0).click();
	        		if(driver.findElement(From_Body).isDisplayed()){
	        			Thread.sleep(5000);
	        			System.out.println("Permitting Form displayed");
	        			driver.findElement(Clickon_BackButton).click();
	        			System.out.println("Permiting testcase succesfully exucated");
	        			Thread.sleep(10000);
	        		}
	        	  }
	        	}
	        	catch(Exception e) {
	        		
	        	}
	        }
	        
	        public void Expand_FID() throws Exception {
	        	List<WebElement> list= driver.findElements(Expand_task);
	        	wdc.waitForPageLoad();
	        	
	        	try {
	        	if(list.get(1).isDisplayed()){
	        		list.get(1).click();
	        		if(driver.findElement(From_Body).isDisplayed()){
	        			Thread.sleep(5000);
	        			System.out.println("FID Form displayed");
	        			driver.findElement(Clickon_BackButton).click();
	        			System.out.println("FID testcase succesfully executed");
	        			Thread.sleep(10000);
	        		}
	        	  }
	        	}
	        	catch(Exception e) {
	        		
	        	}
	        }
	        
	        public void Expand_FEEDProcess() throws Exception {
	        	List<WebElement> list= driver.findElements(Expand_task);
	        	wdc.waitForPageLoad();
	        	
	        	try {
	        	if(list.get(2).isDisplayed()){
	        		list.get(2).click();
	        		if(driver.findElement(From_Body).isDisplayed()){
	        			Thread.sleep(5000);
	        			System.out.println("FEEDProcess Form displayed");
	        			driver.findElement(Clickon_BackButton).click();
	        			System.out.println("FEEDProcess testcase succesfully exucated");
	        		}
	        	  }
	        	}
	        	catch(Exception e) {
	        		
	        	}
	        }
	  }