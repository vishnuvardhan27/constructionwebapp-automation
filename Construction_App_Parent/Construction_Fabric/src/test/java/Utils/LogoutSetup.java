package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogoutSetup extends BaseDriver {

	public LogoutSetup(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);

	@FindBy(xpath = "//div[@id='main-header']//div[@mattooltip='USER PROFILE']")
	public static WebElement UserProfile;
	
	@FindBy(xpath = "//div[contains(text(),'Log Out')]")
	public static WebElement LogOut;
	
	@FindBy(xpath = "//*[@class='logoutTitle']//*[@id='submit']/div")
	public static WebElement Logoutagree;

	By UpdateAttentionPopUPClose = By.xpath("//*[@class='dialogBox']//*[@id='close']/div");
	By UpdateAttentionPopUPSave = By.xpath("//*[@id='save']/div");

	public void UserProfileClick() {

		try {
			wdc.waitForExpectedElementToBeClickable(UserProfile);
			UserProfile.click();
		} catch (Exception e) {

		}
	}

	public void LogOut() {
		try {
			wdc.waitForExpactedElement(LogOut);
			LogOut.click();
		} catch (Exception e) {

		}
	}


	public void Logoutsubmit() {
		try {
			wdc.waitForExpactedElement(Logoutagree);
			Logoutagree.click();
		} catch (Exception e) {

		}
	}

	public void BrowserClose() { // for some days i am keeping in future while running xml file..remove this..
		try {
			driver.close();
		} catch (Exception e) {

		}
	}

	public void LogoutExcute() {
		UserProfileClick();
		LogOut();
		Logoutsubmit();
	}
}