
package Utils;

public class Constant {


	
	public static final String OperationLocation = "INSPECTIONFORMS_CREATION";

	public static final String project = "TestProject";
	public static final String folderName = "Automation_Folder";
	public static final String parentfolderName = "Automation_ParentFolder";
	public static final String Type = "Exhibits";
	public static final String Projectname = "MR GAT";

	public static final String Journals = "Journals";
	public static final String Safety = "Safety";
	public static final String Environment = "Environment";
	public static final String Exhibits = "Exhibits";
	
	public static final String reviewcommant = "Testing comment in review accordion";
	public static final String approvalscomment = "Testing comment in approval accordion";
	public static final String approvalsname = "Automation Testing";
	public static final String reviewby = "Murugan KV";
	
}

