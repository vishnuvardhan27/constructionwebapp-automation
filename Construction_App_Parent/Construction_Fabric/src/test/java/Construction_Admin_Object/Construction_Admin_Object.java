package Construction_Admin_Object;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.Constant;
import Utils.WebDriverCommonLib;

public class Construction_Admin_Object extends BaseDriver{

	public Construction_Admin_Object(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	

	LocalDate localDate = LocalDate.now();
	String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);

	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	
	
	public By CLOSE = By.xpath("//*[@svgicon='V3 CloseCancel']");
	public By UserProfile = By.xpath("//*[@mattooltip='USER PROFILE']");
	public By Construction_Admin = By.xpath("//*[contains(text(),'Construction Admin')]");
	public By AddButton = By.xpath("//*[contains(@name,'plus')]");
	public By FolderName = By.xpath("//*[contains(@name,'foldername')]");
	public By ParentFolderName = By.xpath("//*[contains(@name,'parentfolder')]");
	public By Save = By.xpath("//*[@id='save']//*[contains(@class,'mat-icon')]");
	public By CreatedTurnOverVerify = By.xpath("//*[contains(@class,'array-item')]");
	public By DeleteButton = By.xpath("//*[contains(@name,'delete')]");
	public By AgreeAttention = By.xpath("//*[@id='dragHandler']//*[@id='save']/div");
	public By DeletedTurnOverVerify = By.xpath("//*[contains(text(),'No data to show')]");
	
	public By Approvals = By.xpath("//*[@class='nav-bar']//*[2][@mattooltipposition='right']");
	public By TaggedItems = By.xpath("//*[@class='nav-bar']//*[3][@mattooltipposition='right']");
	public By PhysicalInstance = By.xpath("//*[@class='nav-bar']//*[4][@mattooltipposition='right']");
	public By Materials = By.xpath("//*[@class='nav-bar']//*[5][@mattooltipposition='right']");
	public By Documents = By.xpath("//*[@class='nav-bar']//*[6][@mattooltipposition='right']");
	public By Directory = By.xpath("//*[@class='nav-bar']//*[7][@mattooltipposition='right']");
	public By Procedure = By.xpath("//*[@class='nav-bar']//*[8][@mattooltipposition='right']");
	public By TurnOver = By.xpath("//*[@class='nav-bar']//*[9][@mattooltipposition='right']");
		


	
	
	public void selectProject(String s) throws Exception {
		System.out.println("Trying to click on a Project... ...");
		wdc.waitForPageLoad();
		List<WebElement> nodetext=driver.findElements(By.xpath("//div[@id='Schedule']//*[contains(@class,'nodeText title')]"));	
		List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='Schedule']//*[@class='angular-tree-component']//tree-node"));
		String hightOfTree=driver.findElement(By.xpath("//div[@id='Schedule']//*[contains(@style, 'height')]")).getCssValue("height")
				.replace(" ", "").replaceAll("([a-z])", "");
		System.out.println("Tree hight is after search : "+ hightOfTree);
		int hight=Integer.parseInt(hightOfTree);
		wdc.imlicitlywaitfor_80();
		while (nodetext.size()!=0 && hight<250) {
			for(WebElement e:nodetext) {
				System.out.println("Schedule===="+e.getText());
				Thread.sleep(2000);
				if(e.getText().equals(s)) {			
					actions.click(e).build().perform();
					System.out.println("Schedule clicked.");
					wdc.imlicitlywaitfor_80();
					Thread.sleep(3000);
					break;
				}
			}
			break;
		}
	}
	public void clickOnuserprofilebutton() {
			wdc.waitForExpactedElement_150(driver.findElement(UserProfile));
			actions.click(driver.findElement(UserProfile)).build().perform();
			System.out.println("System clicked on User profile");
			wdc.waitForPageLoad();
	}
	
	public void clickOnConstructionAdmin() {
		    wdc.waitForPageLoad();
			wdc.waitForExpactedElement_150(driver.findElement(Construction_Admin));
			actions.click(driver.findElement(Construction_Admin)).build().perform();
			System.out.println("System clicked on Construction_Admin");
			wdc.waitForPageLoad();
	}
	
	public void clickOnAddButton() throws Exception {
		    wdc.waitForPageLoad();
		    Thread.sleep(3000);
			wdc.waitForExpactedElement_150(driver.findElement(AddButton));
			actions.click(driver.findElement(AddButton)).build().perform();
			System.out.println("System clicked On AddButton");
			wdc.waitForPageLoad();
	}
	public void clickOnFolderName(String s) {
		    wdc.waitForPageLoad();
			wdc.waitForExpactedElement_150(driver.findElement(FolderName));
			actions.click(driver.findElement(FolderName)).sendKeys(s1+" "+s).build().perform();
			System.out.println("System Entered Folder Name");
			wdc.waitForPageLoad();	
	}
	public void clickOnParentFolderName(String s) {
		    wdc.waitForPageLoad();		
			wdc.waitForExpactedElement_150(driver.findElement(ParentFolderName));
			actions.click(driver.findElement(ParentFolderName)).sendKeys(s1+" "+s).sendKeys(Keys.ENTER).build().perform();
			System.out.println("System Entered Parent_FolderName");
			wdc.waitForPageLoad();
	}
	public void selectType(String s) throws Exception {
		Thread.sleep(3000);
		WebElement type = driver.findElement(By.xpath("//*[@role='listbox']//*[text()='Type']"));
		wdc.waitForExpactedElement(type);
		actions.click(type);
		Thread.sleep(3000);
		actions.sendKeys(s)
		.sendKeys(Keys.ENTER)
		.build()
		.perform();
		System.out.println("System selected type from dropdown");
	}
	public void selectReportType() throws Exception {
		Thread.sleep(3000);
		wdc.waitForPageLoad();
		WebElement ReportType = driver.findElement(By.xpath("//*[@role='listbox']//*[text()='Report Type']"));
		wdc.waitForExpactedElement(ReportType);
		actions.click(ReportType);
		wdc.waitForPageLoad();
		List<WebElement> checkBox=driver.findElements(By.xpath("//*[contains(@class, 'multiple-checkbox')]"));
		for (WebElement e: checkBox) {
			e.click();
			wdc.waitForPageLoad();
			break;
		}
	}
	public void clickOnSaveButton() throws Exception {
		Thread.sleep(3000);
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(Save));
		actions.click(driver.findElement(Save)).build().perform();
		wdc.waitForPageLoad();
		Thread.sleep(3000);
		System.out.println("System clicked on save button");
	}
	public void verifyCreatedTurnOver() {
		try {
			wdc.waitForPageLoad();
			if(driver.findElement(CreatedTurnOverVerify).isDisplayed()){
				System.out.println("Admin Created TurnOver is Displaying");
			}
		} catch (Exception e) {
			System.out.println("Admin Created TurnOver is not Displaying");
		}
	}
	
	
	public void clickOnApprovalsTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(Approvals));
		actions.click(driver.findElement(Approvals)).build().perform();
		System.out.println("System Clicked on Approvals Tab");
		wdc.waitForPageLoad();
	}
	public void clickOnTaggedItemsTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(TaggedItems));
		actions.click(driver.findElement(TaggedItems)).build().perform();
		System.out.println("System Clicked on TaggedItems Tab");
		wdc.waitForPageLoad();
	}
	public void clickOnPhysicalInstanceTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(PhysicalInstance));
		actions.click(driver.findElement(PhysicalInstance)).build().perform();
		System.out.println("System Clicked on PhysicalInstance Tab");
		wdc.waitForPageLoad();
	}
	public void clickOnMaterialsTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(Materials));
		actions.click(driver.findElement(Materials)).build().perform();
		System.out.println("System Clicked on Materials Tab");
		wdc.waitForPageLoad();
	}
	public void clickOnDocumentsTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(Documents));
		actions.click(driver.findElement(Documents)).build().perform();
		System.out.println("System Clicked on Documents Tab");
		wdc.waitForPageLoad();
	}
	public void clickOnDirectoryTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(Directory));
		actions.click(driver.findElement(Directory)).build().perform();
		System.out.println("System Clicked on Directory Tab");
		wdc.waitForPageLoad();
	}
	public void clickOnProcedureTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(Procedure));
		actions.click(driver.findElement(Procedure)).build().perform();
		System.out.println("System Clicked on Procedure Tab");
		wdc.waitForPageLoad();
	}
	public void clickOnTurnOverTab() {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(TurnOver));
		actions.click(driver.findElement(TurnOver)).build().perform();
		System.out.println("System Clicked on TurnOver Tab");
		wdc.waitForPageLoad();
	}
	
	
	
	
	public void clickOnDeleteButton() throws Exception {
		Thread.sleep(3000);
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(DeleteButton));
		actions.click(driver.findElement(DeleteButton)).build().perform();
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(AgreeAttention));
		actions.click(driver.findElement(AgreeAttention)).build().perform();
		Thread.sleep(3000);
		System.out.println("TurnOver is Deleted successfully");
		wdc.waitForPageLoad();
	}
	
	public void verifyDeletedturnOver() {
		try {
			wdc.waitForPageLoad();
			if(driver.findElement(DeletedTurnOverVerify).isDisplayed()) {
				System.out.println("Admin Turn Over Deleted Successfully");
			}
		} catch (Exception e) {
			System.out.println("Admin TrunOver is not deleted");
		}
	}
	
	
}
