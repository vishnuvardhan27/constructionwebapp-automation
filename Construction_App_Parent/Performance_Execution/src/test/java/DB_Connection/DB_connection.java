package DB_Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;

public class DB_connection {

Connection con;
	
	public void establishconnection() throws Exception {
		String Driver_Class = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String DBURL = "jdbc:sqlserver://visur.database.windows.net;" + "databaseName=ReportDB;";
        String DBUserName = "visuradmin";
        String DBPassword = "B@*v]c6q_JY]V:H[";
        
        Class.forName(Driver_Class);
        con = DriverManager.getConnection(DBURL,DBUserName,DBPassword);
        con.createStatement();
        System.out.println("connection established");
	}
	
	public void insertintodb(String method, String Fabricname, Double Timetaken,String locationame,String EntityType, String FormType) throws Exception {
	       String sr = "Insert into [dbo].[Construction_UI_Performance_Metrics]([MethodName],[FabricName],[TimeTaken],[DateTime],[LocationName],[EntityType],[FormType]) values(?,?,?,?,?,?,?)";
	        PreparedStatement st = con.prepareStatement(sr,Statement.RETURN_GENERATED_KEYS);

	        st.setString(1, method);
	        st.setString(2, Fabricname);
	        st.setDouble(3, Timetaken);
	        st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	        st.setString(5, locationame);
	        st.setString(6, EntityType);
	        st.setString(7, FormType);
	        int c = st.executeUpdate();
	        System.out.println(c + " record saved");
	        con.close();
	        System.out.println("connection closed");
	}
}
