package Utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;

public class LoginSetup_BI {

	private final DriverManagerType CHROME = null;

	public WebDriver driver;

	public Properties prop = new Properties();
	public String browser;
	public FileInputStream fis;

	By username = By.xpath("//input[@name='username']");
	By nextbutton = By.xpath("//input[@value='NEXT']");
	By password = By.xpath("//input[@id='password']");
	By LOGIN = By.xpath("//input[@value='LOGIN']");
	By blinkingText =By.xpath("//*[text()='VERIFYING DEVICE.']");
	By menubutton =By.xpath("//*[@mattooltip='MENU']");
	By BIbutton = By.xpath("//*[@id='bi']");
	By lefttreeBIfabric = By.xpath("//*[@src='wwwroot/images/V3 Icon Set part2/BusinessIntelligence_white_outlined.svg']");
	By alltab = By.xpath("//*[@class='nav-bar']//div[1]");
	By BusinessIntelligence = By.xpath("//*[@id='Business Intelligence']");
	By AllTab = By.xpath("//*[@class='nav-bar']//*[1][@mattooltipposition='right']");
	By mainHeader = By.xpath("//*[@id='main-header']");
	By oneMomentPlease = By.xpath("//*[text()='One moment please ...']");
	
	public final String companyName = getCompanyName();
	public final String tenantName = getTenantName();
	public String userName = getUserName();
	public String otpname = getOTP();
	public String urlname = getURLName();
	
	public final String GET_URL = "https://qa-enterprises.visur.live:8797/GetOtp?key=" + userName + '@' + companyName + '&' + otpname ;	
//	public final String GET_URL = "http://integration.visur.io:8797/GetOtp?key=" + userName + '@' + tenantName;
	public final String GET_URLQA = "https://qa-enterprises.visur.io:8797/GetOtp?key=" + userName + '@' + tenantName;
	public String OTP;

	By errormessage = By.xpath("//*[@class='error-message']");
	By otppage = By.xpath("//*[@id='otpCode']");

	public void sendGET() throws IOException {
		URL url = null;
		if(urlname.contains("visur.live")) {
			url = new URL(GET_URL);
		} else {
			url = new URL(GET_URLQA);
		}


		HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

		httpURLConnection.setRequestMethod("GET");
		httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0");

		if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
			BufferedReader bufferedReaderInput = new BufferedReader(
					new InputStreamReader(httpURLConnection.getInputStream()));

			String inputLine;

			StringBuffer stringBufferResponse = new StringBuffer();
			while ((inputLine = bufferedReaderInput.readLine()) != null) {
				stringBufferResponse.append(inputLine);
			}
			bufferedReaderInput.close();

			try {
				JSONObject jsonObj = new JSONObject(stringBufferResponse.toString());
				OTP = jsonObj.getString("otp");
				System.out.println(OTP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("GET request not worked");
		}
	}

	@BeforeMethod
	public void LaunchBrowser() throws Exception {

		try {
			getUserName();
			if (prop.getProperty("browser").equals("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "\\driver\\geckodriver.exe");
				driver = new FirefoxDriver();

			} else if (prop.getProperty("browser").equals("chrome")) {

				WebDriverManager.chromedriver().setup();
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
				// BY USING THIS LINE WE ARE ABLE TO GET THE LATEST VERSION OF CHROME DRIVER
				// WebDriverManager.getInstance(CHROME).setup();
				try{
					//BY USING THIS LINE WE ARE ABLE TO GET THE LATEST VERSION OF CHROME DRIVER
					WebDriverManager.getInstance(CHROME).setup(); //by using this line we are able to get the latest version of chromedriver
					//mention the below chrome option to solve timeout exception issue
					ChromeOptions options = new ChromeOptions();
					options.setPageLoadStrategy(PageLoadStrategy.NONE);
					//Run Chrome in incognito mode
					options.addArguments("--incognito");
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					// Instantiate the chrome driver
					driver = new ChromeDriver(options);
					//driver = new ChromeDriver();
				}
				catch(Exception e){

				}

			} else if (prop.getProperty("browser").equals("ie")) {

				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + "\\driver\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();

			}
			Thread.sleep(3000);
			System.out.println(prop.getProperty("URL"));
			driver.get(prop.getProperty("URL"));
			driver.manage().window().maximize();

			System.out.println(prop.getProperty("username"));
			System.out.println(prop.getProperty("Password"));

			driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			driver.findElement(username).sendKeys(prop.getProperty("username"));


			try {
				if(driver.findElement(blinkingText).isDisplayed()) {
					WebDriverWait wait = new WebDriverWait(driver, 80);
					wait.until(ExpectedConditions.invisibilityOf(driver.findElement(blinkingText)));
				}
			} catch (Exception e) {
			}


			driver.manage().timeouts().implicitlyWait(05, TimeUnit.SECONDS);
			driver.findElement(nextbutton).click();

			try {
				if (driver.findElement(errormessage).isDisplayed()) {
					throw new ArithmeticException("errormessage");
				}
			}catch(Exception e) {

			}

			driver.manage().timeouts().implicitlyWait(02, TimeUnit.SECONDS);
			driver.findElement(password).sendKeys(prop.getProperty("Password"));

			Thread.sleep(5000);
			driver.manage().timeouts().implicitlyWait(05, TimeUnit.SECONDS);
			driver.findElement(LOGIN).click();

			try {
				if (driver.findElement(errormessage).isDisplayed()) {
					throw new ArithmeticException("errormessage");
				}
			}
			catch(Exception e) {

			}


			try {
				sendGET();
				if (driver.findElement(otppage).isDisplayed()) {
					driver.findElement(otppage).sendKeys(OTP);
					driver.findElement(By.xpath("//*[@class='rectangle']")).click();
					Thread.sleep(2000);
					try {
						if (driver.findElement(errormessage).isDisplayed()) {
							  Actions actions = new Actions(driver);
							actions.click(driver.findElement(otppage))
							.keyDown(Keys.CONTROL)
							.sendKeys("a")
							.keyUp(Keys.CONTROL)
							.sendKeys(Keys.BACK_SPACE)
							.sendKeys("OTPUsed")
							.sendKeys(Keys.ENTER)
							.sendKeys(Keys.TAB)
							.build()
							.perform();
							
							Thread.sleep(2000);
							driver.findElement(By.xpath("//*[@class='rectangle']")).click();
							Thread.sleep(2000);

							try {
								if (driver.findElement(errormessage).isDisplayed()) {

									driver.findElement(By.xpath("//*[@value ='Resend verification code.']")).click();

									System.out.println("Enter Valid OTP after OTPUsed ");
									sendGET();
									driver.findElement(otppage).sendKeys(OTP);

									Thread.sleep(5000);

									driver.findElement(By.xpath("//*[@class='rectangle']")).click();
								}
							} catch (Exception e) {
							}
						}
					} catch (Exception e) {
					}
				}
			}catch(Exception ex) {

			}
			
			int j=0;
			boolean temp=true;
				do {
					try {
						if (j<2) {
							WebDriverWait wait = new WebDriverWait(driver, 60);
							if (driver.findElement(mainHeader).isDisplayed()) {
								System.out.println("Main header Displayed.");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								wait.until(ExpectedConditions.visibilityOf(driver.findElement(menubutton)));
								if (driver.findElement(menubutton).isDisplayed()) {
									System.out.println("Menu Button displayed.");
									driver.findElement(menubutton).click();
									System.out.println("Menu button clicked.");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id='sub-header']//*[@id='header']"))));
									if (driver.findElement(By.xpath("//*[@id='sub-header']//*[@id='header']")).isDisplayed()) {

										driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
										wait.until(ExpectedConditions.visibilityOf(driver.findElement(BusinessIntelligence)));
										driver.findElement(BusinessIntelligence).click();
										System.out.println("BusinessIntelligence Button clicked.");
										driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
										Thread.sleep(3000);
										if (driver.getCurrentUrl().contains("Business")) {
											System.out.println("Business form displayed.");
											temp=false;
										}
									}
								}
							}
						}
						else {
							System.out.println("Menu button not clicked!");
							temp=false;
						}
					} catch (Exception e) {
						j++;
						driver.navigate().refresh();
						Thread.sleep(5000);
						try {
							if(driver.findElement(oneMomentPlease).isDisplayed()) {
								System.out.println("App loading slowly...");
								WebDriverWait wait = new WebDriverWait(driver, 60);
								wait.until(ExpectedConditions.invisibilityOf(driver.findElement(oneMomentPlease)));
							}
						} catch (Exception e2) {
						}
					}
				} while (temp);
			
			
				/*
				 * Here It'll check BI displayed or not.
				 */
				
				try {
					do {
						driver.findElement(AllTab).click(); 
						System.out.println("All Tab Clicked.");
						try {
							if (driver.findElement(By.xpath("//*[text()=' All ']")).isDisplayed()) {
								System.out.println("Successfully Logged In... ... ...");
								break;
							}
						} catch (Exception e) {
							driver.navigate().refresh();
							Thread.sleep(6000);
						}
					} while (driver.findElement(AllTab).getAttribute("class").equalsIgnoreCase("mat-tooltip-trigger headerIcon ng-star-inserted"));
				} catch (Exception e) {
					System.out.println("All Tab Error!!! !!! !!! !!!");
				}

		}
		catch(Exception e) {
			System.out.println("login..");
		}

	}

	@AfterMethod
	public void LogOut(ITestResult result) {
		try {
			LogoutSetup logoutSetup = new LogoutSetup(driver);
			logoutSetup.LogoutExcute();
			driver.quit();
		} catch (Exception e) {
			try {
				driver.quit();
			} catch (Exception ex) {
			}
		}
	}

	@AfterClass
	public void Quit() {
		try {
			driver.quit();
		} catch (Exception e) {
		}
	}

	public String getUserName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\resources\\TestData.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty("username");
	}
	
	public String getCompanyName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("CompanyName");
	}
	
	public String getOTP() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("OTP");
	}

	public String getTenantName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\resources\\TestData.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty("TenantName");
	}
	
	public String getURLName() {
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + "\\resources\\TestData.Properties");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty("URL");
	}
}
