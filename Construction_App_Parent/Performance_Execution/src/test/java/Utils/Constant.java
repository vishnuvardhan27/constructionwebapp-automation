
package Utils;

public class Constant {
	
	// EM- People
	
	public static final String Automation_Company = "Automation_Performance_Company";
	public static final String Automation_Person = "Automation_Performance _Person";
	
	public static final String EM_People = "EM_People&Companies";
	public static final String BusinessIntelligence = "Business Intelligence";
	public static final String Company = "Company";
	public static final String person = "Person";
	public static final String config = "Config";
	
	// BI
	
	public static final String Integrity_Dig_Sample = "Integrity Dig Sample"; 
	public static final String Lem_Dashboard = "Lem Dashboard";
	
	public static final String bidashboard = "Dashboard";
	public static final String component = "Component Ratio DataSource";
}

