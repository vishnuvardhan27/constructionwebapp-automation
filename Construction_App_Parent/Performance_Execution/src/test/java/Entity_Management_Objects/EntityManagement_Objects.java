package Entity_Management_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class EntityManagement_Objects extends BaseDriver{

	public EntityManagement_Objects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	WebDriverCommonLib wdc=new WebDriverCommonLib(driver);
	Actions actions=new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	
	By peopleAndCompanies_LeftTree= By.xpath("//*[@class='nav-bar']//*[1][@mattooltipposition='right']");
	By Construction_LeftTree= By.xpath("//*[@class='nav-bar']//*[2][@mattooltipposition='right']");
	By DataIsBeingLoaded = By.xpath("//div[@id='People & Companies']/../*[normalize-space(text())='DATA IS BEING LOADED']");
	@FindBy(xpath="//*[@svgicon='LeftPanePlus']")
	public  WebElement clickPlusButton;
	@FindBy(xpath="//*[@class='header-popup']") 
	public  WebElement headerpopup;
	@FindBy(xpath="//*[@class='header-popup']//*[@id='save']/div") 
	public  WebElement headerpopupsave;
	By peopleAndCompanies=By.xpath("//*[@class='nav-bar']//*[1][@mattooltipposition='right']");
	public By companyaccordion = By.xpath("//*[@id='Company']");
	public By personaccordion = By.xpath("//*[@id='Person']");

	
//	public void PeopleAndCompaniesTab() throws Exception{
//		try {
//			do {
//				driver.findElement(peopleAndCompanies).click();
//				System.out.println("People And Companies Clicked.");
//				try {
//					if (driver.findElement(By.xpath("//*[text()=' People & Companies ']")).isDisplayed()) {
//						System.out.println("Successfully Logged In... ... ...");
//						break;
//					}
//				} catch (Exception e) {
//					driver.navigate().refresh();
//					Thread.sleep(6000);
//				}
//			} while (driver.findElement(peopleAndCompanies).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
//		} catch (Exception e) {
//			System.out.println("People And Companies Error!!! !!! !!! !!!");
//		}
//
//	}

	
	public void ClickPlusButton_PeopleAndCompanies() {
		try {
			if(driver.findElement(DataIsBeingLoaded).getText().equalsIgnoreCase("DATA IS BEING LOADED")) {			
				WebDriverWait wait = new WebDriverWait(driver, 80);
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(DataIsBeingLoaded)));
			}
		}
		catch(Exception e) {
			
		}
		try {
			wdc.waitForExpectedElementToBeClickable(clickPlusButton);
			clickPlusButton.click();
			
			if(headerpopup.isDisplayed()) {
				wdc.waitForExpactedElement(headerpopupsave);
				headerpopupsave.click();
			}
		}
		catch(Exception e) {
			
		}
	}
	
	public void clickOnLeftTree_PeoplAndCompanies() {
		try {
//			do {
//				driver.findElement(peopleAndCompanies_LeftTree).click();
//				System.out.println("People & Companies Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' People & Companies ']")).isDisplayed()) {
						System.out.println("Already displaying People & Companies Tab... ... ...");
//						break;
					}
				} catch (Exception e) {
//					driver.navigate().refresh();
//					Thread.sleep(6000);
					driver.findElement(peopleAndCompanies_LeftTree).click();
					System.out.println("People & Companies Tab Clicked.");
				}
//			} while (driver.findElement(peopleAndCompanies_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("People & Companies Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_Construction() {
		try {
			do {
				driver.findElement(Construction_LeftTree).click();
				System.out.println("Construction Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Construction ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Construction... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(Construction_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Construction Error!!! !!! !!! !!!");
		}
	}
	
	public  void ExpandCollapseAll(){
		WebElement selectexpandAll = driver.findElement(By.xpath("//*[@id='expandcollapse']/div/div"));
		actions.click(selectexpandAll).perform();
	}
	
	public void Checkcompanydataload() {
		WebElement company = driver.findElement(companyaccordion);
		
		try{
			if(company.isDisplayed()) {
				System.out.println("Company form is loaded");
			}
		}
		catch(Exception e){
			System.out.println("Company form is not loaded");
		}
	}
	
	public void Checkpersondataload() {
		WebElement person = driver.findElement(personaccordion);
		
		try{
			if(person.isDisplayed()) {
				System.out.println("Person form is loaded");
			}
		}
		catch(Exception e){
			System.out.println("Person form is not loaded");
		}
	}
}


