package log4j2_Implementation;

import org.apache.log4j.xml.DOMConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class log4j_main {
	/**
	 * Initialize Log4j logs
	 */
	private static final Logger logger = LogManager.getLogger(log4j_main.class.getName());
	/**
	 * This is to print log for the beginning of the test case, 
	 * as we usually run so many test cases as a test suite
	 */
	public static void startTestCase(String sTestCaseName){
		 
		logger.info("****************************************************************************************");
		 
		logger.info("****************************************************************************************");
		 
		logger.info("$$$$$$$$$$$$$$$$$$$$$                 "+sTestCaseName+ "       $$$$$$$$$$$$$$$$$$$$$$$$$");
		 
		logger.info("****************************************************************************************");
		 
		logger.info("****************************************************************************************");
		 
		 }
		 
		 /**
		  * This is to print log for the ending of the test case
		  * @param sTestCaseName
		  */
		 
		 public static void endTestCase(String sTestCaseName){
		 
			 logger.info("XXXXXXXXXXXXXXXXXXXXXXX             "+"-E---N---D-"+"             XXXXXXXXXXXXXXXXXXXXXX");
		 
			 logger.info("X");
		 
			 logger.info("X");
		 
			 logger.info("X");
		 
			 logger.info("X");
		 
		 }
		 
		 /**   
		  * Need to create these methods, so that they can be called
		  * @param message
		  */
		 
		 public static void info(String message) {
			 logger.info(message);
		 }
		 
		 public static void warn(String message) {
			 logger.warn(message);
		 }
		 
		 public static void error(String message) {
			 logger.error(message);
		 }
		 
		 public static void fatal(String message) {
			 logger.fatal(message);
		 }
		 
		 public static void debug(String message) {
			 logger.debug(message);
		 }
		 
		 /**
		  * method to init log4j configurations, should be called first before using logging
		  */
		 public static void init() {
		 	DOMConfigurator.configure(System.getProperty("user.dir") + "\\src\\main\\resources\\log4j.xml");
		 	// OR for property file, should use any one of these
		 	//PropertyConfigurator.configure("myapp-log4j.properties");
		 }
}
