
package SearchEntities;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import DragAndDrop_POM.Drag_and_Drop;
import Security_Construction_Objects.Security_Construction_Objects;
import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Searchentitiesobjects extends BaseDriver{

	public Searchentitiesobjects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	LocalDate localDate = LocalDate.now();
	String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);

	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	Drag_and_Drop drag_and_Drop=new Drag_and_Drop(driver);
	Security_Construction_Objects construction_Role_Objects=new Security_Construction_Objects(driver);
	
	public String production,month ;
	WebElement source = null;

	By SearchIcon = By.xpath("//*[@id='filter-search']//mat-icon[@svgicon='MainSearch']");
	By rightTree_SearchIcon=By.xpath("//*[@id='right-filter-search']//mat-icon[@svgicon='MainSearch']");
	By rightTree_FilterBox=By.xpath("//*[@id='right-filter-search']//*[@placeholder='  Filter']");
	By FilterTextBox = By.xpath("//*[@placeholder='   Filter']");
	By CLOSE = By.xpath("//*[@svgicon='V3 CloseCancel']");
	By peopleAndCompaniesDataIsBeingLoaded = By.xpath("//div[@id='People & Companies']/../*[normalize-space(text())='DATA IS BEING LOADED']");
	By FormName = By.xpath("//*[@id='fdc-data-entry']//label");
	By productiondate = By.xpath("//*[@class='location-header']");
	By construction_left_Filterbox=By.xpath("//*[@class='leftTreeRightArea']");
	
	


	public  void CLOSE(){
		driver.findElement(CLOSE).click(); 
	}
	
	public  void Searchicon(){
		actions.click(driver.findElement(SearchIcon)).perform();
	}
	
	public  void filterinput(String s){
		wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
		actions.click(driver.findElement(FilterTextBox)).sendKeys(s).sendKeys(Keys.ENTER).perform();
	}
	
	public  void SearchEntity(String s) throws Exception{
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					clickOnSearch();
					clickOnFilterBox(s);
					wdc.waitForPageLoad();	
					clickOnLocation(s);
					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[contains(@id, 'form-header')]")).isDisplayed() && driver.findElement(By.xpath("//*[@id='form-generation']")).isDisplayed()) {
						System.out.println("Project form displayed.");
						obj=false;
					}
				}
				catch (Exception e) {
					i++;
					driver.navigate().refresh();
					System.out.println("refreshed in Search method.");
					Thread.sleep(10000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}	
	
	
//	public void searchAndDragAndDrop(String s, WebElement destination) throws Exception {
//		boolean obj=true;
//		int i=0;
//		while (obj) {
//			if (i<3) {
//				try {
//					System.out.println("search");
//					waitForDataLoading();
//					clickOnSearch();
//					clickOnFilterBox(s);
//					wdc.waitForPageLoad();	
//					//clickOnLocation(s);
//					System.out.println("Trying to drag and drop a entity... ...");
//					wdc.waitForPageLoad();
//					List<WebElement> nodetext=driver.findElements(By.xpath("//div[@id='Construction Role']//*[contains(@class,'nodeText')]"));	
//					List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='Construction Role']//*[contains(@class,'node-content-wrapper')]"));
//					String hightOfTree=driver.findElement(By.xpath("//div[@id='Construction Role']//*[contains(@style, 'height')]")).getCssValue("height")
//							.replace(" ", "").replaceAll("([a-z])", "");
//					System.out.println("Tree hight is after search : "+ hightOfTree);
//					int hight=Integer.parseInt(hightOfTree);
//					wdc.imlicitlywaitfor_80();
//					int count=0;
//					while (nodetext.size()!=0 && hight<250) {
//						for(WebElement e:nodetext) {
//							System.out.println("Construction Role==== "+e.getText());
//							Thread.sleep(2000);
//							if(e.getText().equals(s)) {	
//								e.click();
//								drag_and_Drop.drag_and_drop(e, destination);
//								System.out.println("Entity Dragged & Dropped.");
//								wdc.imlicitlywaitfor_80();
//								Thread.sleep(3000);
//								break;
//							}
//							count++;
//						}
//						break;
//					}
//					wdc.imlicitlywaitfor_80();
//					if (driver.findElement(By.xpath("//*[contains(@class, 'expansionheader')]")).isDisplayed()) {
//						System.out.println("Entity Dragged & Dropped successfully.");
//						obj=false;
//					}
//				}
//				catch (Exception e) {
//					i++;
//					driver.navigate().refresh();
//					System.out.println("refreshed in Search method.");
//					Thread.sleep(10000);	
//				}
//			}
//			else {
//				obj=false;
//				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
//			}
//		}
//	}
//	
	public void clickOnSearch() throws Exception {
		wdc.waitForExpactedElement(driver.findElement(SearchIcon));
		if (driver.findElement(SearchIcon).isDisplayed()) {
			System.out.println("Search icon displayed.");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			actions.click(driver.findElement(SearchIcon)).build().perform();
			System.out.println("searchicon");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	
	public void clickOnFilterBox(String s) throws Exception {
		wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
		if (driver.findElement(FilterTextBox).isDisplayed()) {
			System.out.println("Filter Box Displayed.");
			wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
			actions.click(driver.findElement(FilterTextBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
			wdc.waitForPageLoad();
			Thread.sleep(2000);
		}
	}
	
	public void clickOnLocation(String s) throws Exception {
		System.out.println("Trying to click on a location... ...");
		wdc.waitForPageLoad();
		List<WebElement> nodetext=driver.findElements(By.xpath("//div[@id='People & Companies']//*[contains(@class,'nodeText title')]"));	
		List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
		String hightOfTree=driver.findElement(By.xpath("//div[@id='People & Companies']//*[contains(@style, 'height')]")).getCssValue("height")
				.replace(" ", "").replaceAll("([a-z])", "");
		System.out.println("Tree hight is after search : "+ hightOfTree);
		int hight=Integer.parseInt(hightOfTree);
		wdc.imlicitlywaitfor_80();
		while (nodetext.size()!=0 && hight<250) {
			for(WebElement e:nodetext) {
				System.out.println("People&Companies===="+e.getText());
				Thread.sleep(2000);
				if(e.getText().equals(s)) {			
					actions.click(e).build().perform();
					System.out.println("People And Companies clicked.");
					actions.click(driver.findElement(CLOSE)).build().perform();
					wdc.imlicitlywaitfor_80();
					Thread.sleep(3000);
					break;
				}
			}
			break;
		}
	}
	
	public void waitForDataLoading() {
		try {
			wdc.waitForPageLoad();
			if(driver.findElement(peopleAndCompaniesDataIsBeingLoaded).isDisplayed()) {			
				System.out.println("DataIsBeingLoaded is displaying");
				WebDriverWait wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(peopleAndCompaniesDataIsBeingLoaded)));
			}
		}
		catch(Exception e) {
			System.out.println("DATA IS BEING LOADED is not displaying");
		}
	}

	
	
	public void searchAndDelete_Role(String s) throws Exception {
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					wdc.waitForExpactedElement(driver.findElement(rightTree_SearchIcon));
					if (driver.findElement(rightTree_SearchIcon).isDisplayed()) {
						System.out.println("Role Search icon displayed.");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						actions.click(driver.findElement(rightTree_SearchIcon)).build().perform();
						System.out.println("searchicon");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}
					
					
					wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
					if (driver.findElement(rightTree_FilterBox).isDisplayed()) {
						System.out.println("Role Filter Box Displayed.");
						wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
						actions.click(driver.findElement(rightTree_FilterBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
						wdc.waitForPageLoad();
						Thread.sleep(2000);
					}
					
					wdc.waitForPageLoad();	


					
					System.out.println("Trying to Right click on a Role... ...");
					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@class,'nodeText title')]"));	
					//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@style, 'height')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("Role==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equals(s)) {			
								actions.contextClick(e).build().perform();
								System.out.println("Role right clicked.");
								wdc.imlicitlywaitfor_80();
								Thread.sleep(3000);
								break;
							}
						}
						break;
					}
					
					
					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[@role='menuitem']")).isDisplayed() && driver.findElement(By.xpath("//*[text()='Delete']")).isDisplayed()) {
						System.out.println("Delete option displayed.");
						driver.findElement(By.xpath("//*[text()='Delete']")).click();
						obj=false;
					}
				}
				catch (Exception e) {
					Thread.sleep(10000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}
	public void searchAndclickRole(String s) throws Exception {
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					wdc.waitForExpactedElement(driver.findElement(rightTree_SearchIcon));
					if (driver.findElement(rightTree_SearchIcon).isDisplayed()) {
						System.out.println("Role Search icon displayed.");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						actions.click(driver.findElement(rightTree_SearchIcon)).build().perform();
						System.out.println("searchicon");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}
					
					
					wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
					if (driver.findElement(rightTree_FilterBox).isDisplayed()) {
						System.out.println("Role Filter Box Displayed.");
						wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
						actions.click(driver.findElement(rightTree_FilterBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
						wdc.waitForPageLoad();
						Thread.sleep(2000);
					}
					
					wdc.waitForPageLoad();	


					
					System.out.println("Trying to Right click on a Role... ...");
					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@class,'nodeText title')]"));	
					//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@style, 'height')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("Role==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equals(s)) {			
								actions.click(e).build().perform();
								System.out.println("Role clicked.");
								wdc.imlicitlywaitfor_80();
								Thread.sleep(3000);
								break;
							}
						}
						break;
					}
					
					
					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[@class='permissionsRoleCreator']")).isDisplayed()) {
						System.out.println("Role form displayed.");
						obj=false;
					}
				}
				catch (Exception e) {
					Thread.sleep(10000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}
	
	public void searchAndclick_Construction_project(String s) throws Exception {
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					wdc.waitForExpactedElement(driver.findElement(rightTree_SearchIcon));
					if (driver.findElement(SearchIcon).isDisplayed()) {
						System.out.println(" Search icon displayed.");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						actions.click(driver.findElement(SearchIcon)).build().perform();
						System.out.println("searchicon");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}
					
					
					wdc.waitForExpactedElement(driver.findElement(construction_left_Filterbox));
					if (driver.findElement(construction_left_Filterbox).isDisplayed()) {
						System.out.println("Construction Filter Box Displayed.");
						wdc.waitForExpactedElement(driver.findElement(construction_left_Filterbox));
						actions.click(driver.findElement(construction_left_Filterbox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
						wdc.waitForPageLoad();
						Thread.sleep(2000);
					}
					
					wdc.waitForPageLoad();	


					
					System.out.println("Trying to  click on a project... ...");
					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//*[@class='node-wrapper']//*[contains(@class,'nodeText title')]"));	
					//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//*[@class='ng-star-inserted']//*[contains(@style, 'height: 102px;')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("project==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equals(s)) {			
								actions.click(e).build().perform();
								System.out.println("project clicked.");
								wdc.imlicitlywaitfor_80();
								Thread.sleep(3000);
								break;
							}
						}
						break;
					}
					
					
					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[@class='landing-page-playing-area full-view']")).isDisplayed()) {
						System.out.println("Security form displayed.");
						obj=false;
					}
				}
				catch (Exception e) {
					Thread.sleep(10000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}


public void searchAndclickGroup(String s) throws Exception {
	boolean obj=true;
	int i=0;
	while (obj) {
		if (i<3) {
			try {
				System.out.println("search");
				waitForDataLoading();
				wdc.waitForExpactedElement(driver.findElement(rightTree_SearchIcon));
				if (driver.findElement(rightTree_SearchIcon).isDisplayed()) {
					System.out.println("Group Search icon displayed.");
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					actions.click(driver.findElement(rightTree_SearchIcon)).build().perform();
					System.out.println("searchicon");
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				}
				
				
				wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
				if (driver.findElement(rightTree_FilterBox).isDisplayed()) {
					System.out.println("Group Filter Box Displayed.");
					wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
					actions.click(driver.findElement(rightTree_FilterBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
					wdc.waitForPageLoad();
					Thread.sleep(2000);
				}
				
				wdc.waitForPageLoad();	


				
				System.out.println("Trying to Right click on a Group... ...");
				wdc.waitForPageLoad();
				List<WebElement> nodetext=driver.findElements(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@class,'nodeText title')]"));	
				//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
				String hightOfTree=driver.findElement(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@style, 'height')]")).getCssValue("height")
						.replace(" ", "").replaceAll("([a-z])", "");
				System.out.println("Tree hight is after search : "+ hightOfTree);
				int hight=Integer.parseInt(hightOfTree);
				wdc.imlicitlywaitfor_80();
				while (nodetext.size()!=0 && hight<250) {
					for(WebElement e:nodetext) {
						System.out.println("Group==== "+e.getText());
						Thread.sleep(2000);
						if(e.getText().equals(s)) {			
							actions.click(e).build().perform();
							System.out.println("Group clicked.");
							wdc.imlicitlywaitfor_80();
							Thread.sleep(3000);
							break;
						}
					}
					break;
				}
				
				
				wdc.imlicitlywaitfor_80();
				if (driver.findElement(By.xpath("//*[@class='securityGroupsMain']")).isDisplayed()) {
					System.out.println("Group form displayed.");
					obj=false;
				}
			}
			catch (Exception e) {
				Thread.sleep(10000);	
			}
		}
		else {
			obj=false;
			System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
		}
	}
  }
public void searchAndDelete_Group(String s) throws Exception {
	boolean obj=true;
	int i=0;
	while (obj) {
		if (i<3) {
			try {
				System.out.println("search");
				waitForDataLoading();
				wdc.waitForExpactedElement(driver.findElement(rightTree_SearchIcon));
				if (driver.findElement(rightTree_SearchIcon).isDisplayed()) {
					System.out.println("Group Search icon displayed.");
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					actions.click(driver.findElement(rightTree_SearchIcon)).build().perform();
					System.out.println("searchicon");
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				}
				
				
				wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
				if (driver.findElement(rightTree_FilterBox).isDisplayed()) {
					System.out.println("Group Filter Box Displayed.");
					wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
					actions.click(driver.findElement(rightTree_FilterBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
					wdc.waitForPageLoad();
					Thread.sleep(2000);
				}
				
				wdc.waitForPageLoad();	


				
				System.out.println("Trying to Right click on a Group... ...");
				wdc.waitForPageLoad();
				List<WebElement> nodetext=driver.findElements(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@class,'nodeText title')]"));	
				//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
				String hightOfTree=driver.findElement(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@style, 'height')]")).getCssValue("height")
						.replace(" ", "").replaceAll("([a-z])", "");
				System.out.println("Tree hight is after search : "+ hightOfTree);
				int hight=Integer.parseInt(hightOfTree);
				wdc.imlicitlywaitfor_80();
				while (nodetext.size()!=0 && hight<250) {
					for(WebElement e:nodetext) {
						System.out.println("Group==== "+e.getText());
						Thread.sleep(2000);
						if(e.getText().equals(s)) {			
							actions.contextClick(e).build().perform();
							System.out.println("Group right clicked.");
							wdc.imlicitlywaitfor_80();
							Thread.sleep(3000);
							break;
						}
					}
					break;
				}
				
				
				wdc.imlicitlywaitfor_80();
				if (driver.findElement(By.xpath("//*[@role='menuitem']")).isDisplayed() && driver.findElement(By.xpath("//*[text()='Delete']")).isDisplayed()) {
					System.out.println("Delete option displayed.");
					driver.findElement(By.xpath("//*[text()='Delete']")).click();
					obj=false;
				}
			}
			catch (Exception e) {
				Thread.sleep(10000);	
			}
		}
		else {
			obj=false;
			System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
		}
	}
}
}