package Security_Common_POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Security_Common_Objects extends BaseDriver{

	public Security_Common_Objects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	WebDriverCommonLib wdc=new WebDriverCommonLib(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	
	By peopleAndCompanies_LeftTree= By.xpath("//*[@class='nav-bar']//*[1][@mattooltipposition='right']");
	By Construction_LeftTree= By.xpath("//*[@class='nav-bar']//*[2][@mattooltipposition='right']");
	By DataIsBeingLoaded = By.xpath("//div[@id='People & Companies']/../*[normalize-space(text())='DATA IS BEING LOADED']");
	@FindBy(xpath="//*[@svgicon='LeftPanePlus']")
	public  WebElement clickPlusButton;
	@FindBy(xpath="//*[@class='header-popup']") 
	public  WebElement headerpopup;
	@FindBy(xpath="//*[@class='header-popup']//*[@id='save']/div") 
	public  WebElement headerpopupsave;
	
	
	public void ClickPlusButton_PeopleAndCompanies() {
		try {
			if(driver.findElement(DataIsBeingLoaded).getText().equalsIgnoreCase("DATA IS BEING LOADED")) {			
				WebDriverWait wait = new WebDriverWait(driver, 80);
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(DataIsBeingLoaded)));
			}
		}
		catch(Exception e) {
		}
		try {
			wdc.waitForExpectedElementToBeClickable(clickPlusButton);
			clickPlusButton.click();
			if(headerpopup.isDisplayed()) {
				wdc.waitForExpactedElement(headerpopupsave);
				headerpopupsave.click();
			}
		}
		catch(Exception e) {
		}
	}
	
	public void clickOnLeftTree_PeoplAndCompanies() {
		try {
			do {
				driver.findElement(peopleAndCompanies_LeftTree).click();
				System.out.println("People & Companies Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' People & Companies ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on People & Companies... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(peopleAndCompanies_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("People & Companies Error!!! !!! !!! !!!");
		}
	}
	
	public void clickOnLeftTree_Construction() {
		try {
			do {
				driver.findElement(Construction_LeftTree).click();
				System.out.println("Construction Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Construction ']")).isDisplayed()) {
						System.out.println("Successfully Clicked on Construction... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(Construction_LeftTree).getAttribute("class").equalsIgnoreCase("headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("Construction Error!!! !!! !!! !!!");
		}
	}
}
