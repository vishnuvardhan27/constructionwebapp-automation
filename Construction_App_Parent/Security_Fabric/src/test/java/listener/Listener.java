package listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Listener implements ITestListener{

	@Override
	public void onTestStart(ITestResult result) {
		
		String testcasename = result.getName();
		System.out.println(testcasename +"=============="+ "testcase started.... ");
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		String testcasename = result.getName();
		System.out.println(testcasename +"=============="+ "testcase passed.... ");
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		String testcasename = result.getName();
		System.out.println(testcasename +"=============="+ "testcase failed.... ");
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		String testcasename = result.getName();
		System.out.println(testcasename +"=============="+ "testcase Skipped.... ");
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		String testcasename = result.getName();
		System.out.println(testcasename +"=============="+ "testcase Failed But With in Success Percentage.... ");
		
	}

	@Override
	public void onStart(ITestContext context) {
		String testcasename = context.getName();
		System.out.println(testcasename +"=============="+ "testcase Start.... ");
		
	}

	@Override
	public void onFinish(ITestContext context) {
		String testcasename = context.getName();
		System.out.println(testcasename +"=============="+ "testcase Finished.... ");
		
	}


}

