package Security_Construction_Scripts;

import org.testng.annotations.Test;

import DragAndDrop_POM.Drag_and_Drop;
import SearchEntities.Searchentitiesobjects;
import Security_Construction_Objects.Security_Construction_Objects;
import Utils.Constant;
import Utils.LoginSetup;

public class Security_Construction_Role_Script extends LoginSetup{
	@Test(groups = {"Sanity"})
	public void security_Construction_Role_Script() throws Exception {	
		System.out.println("Security_Construction_Role_Script----Execution Started");
		
		Security_Construction_Objects construction_Role_Objects=new Security_Construction_Objects(driver);
		Drag_and_Drop drag_and_Drop=new Drag_and_Drop(driver);
		Searchentitiesobjects searchentitiesobjects=new Searchentitiesobjects(driver);
		
		construction_Role_Objects.Clickon_Construction();
		construction_Role_Objects.Clickon_SecurityRoles();
		construction_Role_Objects.Clickon_Addbutton();
		construction_Role_Objects.Clickon_Enter_rollname(Constant.EnterRolename);
		construction_Role_Objects.Clickon_SaveButton();
		construction_Role_Objects.dragRoleForm();
		System.out.println("darg and drop succesfully");
		construction_Role_Objects.Clickon_SaveButton();
		construction_Role_Objects.toggleOnCreateButton();
		construction_Role_Objects.Clickon_SaveButton();
		construction_Role_Objects.checktoggleCreateOn();
		construction_Role_Objects.Clickon_SaveButton();
		construction_Role_Objects.toggleOFFCreateButton(); 	
		construction_Role_Objects.Clickon_SaveButton();
		construction_Role_Objects.toggleOFFReadButton();
		construction_Role_Objects.Clickon_SaveButton();
		construction_Role_Objects.clickOnRoleCloseButton();
		searchentitiesobjects.searchAndclickRole(Constant.EnterRolename);
		construction_Role_Objects.EntityNameUpdate();
		construction_Role_Objects.Clickon_SaveButton();
		construction_Role_Objects.deleteForm();
		construction_Role_Objects.Clickon_SaveButton();
		
		searchentitiesobjects.searchAndDelete_Role(Constant.EditRolename);
		construction_Role_Objects.clickOnDeleteButton();
		
		System.out.println("Security_Construction_Role_Script----Execution Succesfull");
		
	}
}
