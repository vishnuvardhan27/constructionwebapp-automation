package Security_Construction_Scripts;
import org.testng.annotations.Test;

import SearchEntities.Searchentitiesobjects;
import Security_Construction_Objects.Security_Construction_Objects;
import Utils.Constant;
import Utils.LoginSetup;

public class Security_Construction_ScheduleGroup_Script extends LoginSetup {

	@Test(groups = {"Sanity"})
	public void security_Construction_ScheduleGroup_Script() throws Exception {
		System.out.println("Security_Construction_ScheduleGroup_Script---------Execution Started");
		Security_Construction_Objects security_Construction_Objects=new Security_Construction_Objects(driver);
		Searchentitiesobjects searchentitiesobjects=new Searchentitiesobjects(driver);
		
		security_Construction_Objects.Clickon_Construction();
		searchentitiesobjects.searchAndclick_Construction_project(Constant.Projectname);
		security_Construction_Objects.Clickon_Addbutton();
		security_Construction_Objects.Clickon_Enter_Security_Groupname(Constant.GroupName);
		security_Construction_Objects.clickAndSelect_Entitytype(Constant.ScheduleEntity_type);
		security_Construction_Objects.Clickon_right_saveButton();
		security_Construction_Objects.clickOnGroupCloseButton();
		searchentitiesobjects.searchAndclickGroup(Constant.GroupName);
		security_Construction_Objects.verifyCreatedSecurityGroup();
		security_Construction_Objects.updateSecurityGroupName(Constant.UpdateGroupName);
		security_Construction_Objects.Clickon_right_saveButton();
		searchentitiesobjects.searchAndDelete_Group(Constant.UpdateGroupName);
		security_Construction_Objects.clickOnDeleteButton();
		System.out.println("Security_Construction_ScheduleGroup_Script---------Execution Successfull");
		
	}
}
