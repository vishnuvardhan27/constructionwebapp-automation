
package Utils;

public class Constant {


	
	public static final String OperationLocation = "INSPECTIONFORMS_CREATION";
	public static final String EnterRolename="Automation_Role";
	public static final String role="Above Ground Conduit Inspection";
    public static final String EditRolename="Automation_Role_updated";
    public static final String Projectname="TestProject";
    public static final String GroupName="Automation_Group";
    public static final String UpdateGroupName="Automation_Group_Update";
    public static final String ScheduleEntity_type="Schedule";
    public static final String TaggedItemsEntity_type="Tagged Items";
    public static final String MaterialsEntity_type="Materials";

    
    
}

