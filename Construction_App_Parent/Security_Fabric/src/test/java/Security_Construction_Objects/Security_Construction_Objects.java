package Security_Construction_Objects;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.Constant;
import Utils.WebDriverCommonLib;

public class Security_Construction_Objects extends BaseDriver{

	public Security_Construction_Objects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	WebDriverCommonLib wdc=new WebDriverCommonLib(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	Actions actions=new Actions(driver);




	public By Clickon_Construction=By.xpath("//*[@class='nav-bar']//*[2][@mattooltipposition='right']");
	public By Clickon_SecurityRoles=By.xpath("//*[@class='nav-bar']//*[1][@mattooltipposition='left']");
	public By Clickon_Addbutton=By.xpath("//*[contains(@class,'AddNewIcon')] //*[@svgicon='LeftPanePlus'] ");
	public By Clickon_Enter_rollname=By.xpath("//*[@class='role-header']//*[@placeholder='Enter Role Name']");
	public By Clickon_SaveButton=By.xpath("//*[@class='role-header']//*[contains(@class,'right SaveIcon ng-star-inserted')]");
	public By destinationLOCRole=By.xpath("//*[@class='permissionsRoleCreator']//*[@class='container-body']");
	public By destinationform=By.xpath("//div[@class='fabric-area']//*[contains(@class,'scroll-y scrollbar')]");
	public By checkCreateButton=By.xpath("//div[@class='mat-slide-toggle-thumb']");
	public By RoleForm = By.xpath("//div[@id='Construction Role']//*[contains(@class,'nodeText')]//../..//..//*[@class='e-fullrow']");
	public By DeleteButton = By.xpath("//*[@id='dialogContent']");
	public By AgreeAttention = By.xpath("//*[@id='dragHandler']//*[@id='save']/div");
	public By ClickonRoleCloseButton=By.xpath("//*[contains(@class,'CLOSE')]//*[contains(@class,'notranslate')]");
	public By Security_GroupName=By.xpath("//*[contains(@class,'group')]//*[contains(@placeholder,'Group Name')]");
	public By Clickon_right_saveButton=By.xpath("//*[contains(@class,'Security')]//*[contains(@class,'SaveIcon')]");
	public By clickOn_Entitytype=By.xpath("//*[contains(@class,'Security')]//*[text()='Entity Type']");
	public By Entitytype=By.xpath("//*[@class='ng-option ng-star-inserted ng-option-marked']//*[contains(@ng-reflect-ng-item-label,'Schedule')]");
	public By ClickonGroupCloseButton =By.xpath("//*[@class='mat-tooltip-trigger head-route right']//*[contains(@class,'notranslate')]");  
	

	   public  void Clickon_Construction() throws Exception {
		WebElement EntityType=driver.findElement(Clickon_Construction);
		wdc.waitForExpactedElement(EntityType);
		actions.click(EntityType).build().perform();		
		System.out.println("clicked on Entity type");
		Thread.sleep(3000);
	}
	public void Clickon_SecurityRoles() throws Exception {
		WebElement EntityType=driver.findElement(Clickon_SecurityRoles);
		wdc.waitForExpactedElement(EntityType);
		actions.click(EntityType).build().perform();		
		System.out.println("clicked on SecurityRoles");
		Thread.sleep(4000);
	}

	public void Clickon_Addbutton() throws Exception {
		WebElement EntityType=driver.findElement(Clickon_Addbutton);
		wdc.waitForExpactedElement(EntityType);
		actions.click(EntityType).build().perform();		
		System.out.println("clicked on Addbutton");
		Thread.sleep(3000);
	}
	public void Clickon_Enter_rollname(String s) throws Exception {
		WebElement EntityType=driver.findElement(Clickon_Enter_rollname);
		wdc.waitForExpactedElement(EntityType);
		actions.click(EntityType).sendKeys(s).build().perform();		
		System.out.println("clicked on EnterRolename");
		Thread.sleep(10000);
	}

	public void Clickon_SaveButton() throws Exception {
		WebElement EntityType=driver.findElement(Clickon_SaveButton);
		wdc.waitForExpactedElement(EntityType);
		actions.click(EntityType).build().perform();		
		System.out.println("clicked on Savebutton");
		Thread.sleep(4000);
		
	}
	public void clickOnRoleCloseButton() throws Exception {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement(driver.findElement(ClickonRoleCloseButton));
		actions.click(driver.findElement(ClickonRoleCloseButton)).build().perform();		
		System.out.println("clicked on closebutton");
		Thread.sleep(4000);
	}

	public void drag_and_drop(WebElement source,WebElement Destination) {

		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("function createEvent(typeOfEvent) {\n" + "var event =document.createEvent(\"CustomEvent\");\n"
				+ "event.initCustomEvent(typeOfEvent,true, true, null);\n" + "event.dataTransfer = {\n" + "data: {},\n"
				+ "setData: function (key, value) {\n" + "this.data[key] = value;\n" + "},\n"
				+ "getData: function (key) {\n" + "return this.data[key];\n" + "}\n" + "};\n" + "return event;\n"
				+ "}\n" + "\n" + "function dispatchEvent(element, event,transferData) {\n"
				+ "if (transferData !== undefined) {\n" + "event.dataTransfer = transferData;\n" + "}\n"
				+ "if (element.dispatchEvent) {\n" + "element.dispatchEvent(event);\n"
				+ "} else if (element.fireEvent) {\n" + "element.fireEvent(\"on\" + event.type, event);\n" + "}\n"
				+ "}\n" + "\n" + "function simulateHTML5DragAndDrop(element, destination) {\n"
				+ "var dragStartEvent =createEvent('dragstart');\n" + "dispatchEvent(element, dragStartEvent);\n"
				+ "var dropEvent = createEvent('drop');\n"
				+ "dispatchEvent(destination, dropEvent,dragStartEvent.dataTransfer);\n"
				+ "var dragEndEvent = createEvent('dragend');\n"
				+ "dispatchEvent(element, dragEndEvent,dropEvent.dataTransfer);\n" + "}\n" + "\n"
				+ "var source = arguments[0];\n" + "var destination = arguments[1];\n"
				+ "simulateHTML5DragAndDrop(source,destination);", source, Destination);

	}



	
	public void toggleOnCreateButton() throws Exception{
		List<WebElement> checkCreateButton = driver.findElements(By.xpath("//*[@class='role-header']//div[contains(@class,'mat-slide-toggle')]//*[@type='checkbox']"));
		WebElement createToggleButton=checkCreateButton.get(0);
		List<WebElement> ToggleButtonList = driver.findElements(By.xpath("//*[@class='role-header']//div[@class='mat-slide-toggle-thumb']"));
		
		wdc.waitForPageLoad();
		try{
			if(createToggleButton.getAttribute("aria-checked").equals("false")){
				System.out.println("Passed -- Create Button is displayed off state in location data entry form");
				ToggleButtonList.get(0).click();
				wdc.waitForPageLoad();
			}
		}
		catch(Exception e){
			System.out.println("Create button on testcase failed");
		}
		Thread.sleep(4000);
	}
		
	//Check create button on or off
		
		public void checktoggleCreateOn() throws Exception{

			List<WebElement> checkCreateButton = driver.findElements(By.xpath("//*[@class='role-header']//div[contains(@class,'mat-slide-toggle')]//*[@type='checkbox']"));
			WebElement createToggleButton=checkCreateButton.get(0);


			try{
				if(createToggleButton.getAttribute("aria-checked").equals("true")){
					System.out.println("Passed -- Create Button is displayed on state in location data entry form");
				}
			}
			catch(Exception e){
				System.out.println("Create button on testcase failed");
			}
			Thread.sleep(4000);
		}
		
		
		
		public void dragRoleForm() throws Exception {
		for(int i=0; i<6;i++) {
			if(i!=0) {
				Thread.sleep(2000);
				List<WebElement> list=driver.findElements(RoleForm);
				drag_and_drop(list.get(i),driver.findElement(destinationLOCRole));
				Thread.sleep(4000);
			}
		   }
		}
		
		
	//create toggle button off
		public void toggleOFFCreateButton() throws Exception{
			List<WebElement> checkCreateButton = driver.findElements(By.xpath("//*[@class='role-header']//div[contains(@class,'mat-slide-toggle')]//*[@type='checkbox']"));
			WebElement createToggleButton=checkCreateButton.get(0);
			List<WebElement> ToggleButtonList = driver.findElements(By.xpath("//*[@class='role-header']//div[@class='mat-slide-toggle-thumb']"));
			
			wdc.waitForPageLoad();
			try{
				if(createToggleButton.getAttribute("aria-checked").equals("true")){
					System.out.println("Passed -- Create Button is in on state.");
					ToggleButtonList.get(0).click();
					wdc.waitForPageLoad();
				}
			}
			catch(Exception e){
				System.out.println("Create button on testcase failed");
			}
			Thread.sleep(4000);
		}
		
		//Read toggle button off
		public void toggleOFFReadButton() throws Exception{
			List<WebElement> checkReadButton = driver.findElements(By.xpath("//*[@class='role-header']//div[contains(@class,'mat-slide-toggle')]//*[@type='checkbox']"));
			WebElement createToggleButton=checkReadButton.get(1);
			List<WebElement> ToggleButtonList = driver.findElements(By.xpath("//*[@class='role-header']//div[@class='mat-slide-toggle-thumb']"));
			
			wdc.waitForPageLoad();
			try{
				if(createToggleButton.getAttribute("aria-checked").equals("true")){
					System.out.println("Passed -- Read Button is in on state.");
					ToggleButtonList.get(1).click();
					wdc.waitForPageLoad();
				}
			}
			catch(Exception e){
				System.out.println("Read button on testcase failed");
			}
			Thread.sleep(4000);
			
		}
		
		//Delete the form
		
		public void deleteForm()
		{
			int i=0;
		    List<WebElement> ToogleButtonList=driver.findElements(By.xpath("//*[@class='deleteSvg']//*[@data-mat-icon-name='FDC_Delete_Icon']"));
		    for(WebElement e:ToogleButtonList) {
		    	try {
					if (ToogleButtonList.size()>0) {
						ToogleButtonList.get(i).click();
						if (driver.findElement(By.xpath("//*[@id='save']//*[contains(@class,'head-route inactive-icon-opacity')]")).isDisplayed()){
							driver.findElement(By.xpath("//*[@id='save']//*[contains(@class,'head-route inactive-icon-opacity')]")).click();
						}
					}
				} catch (Exception e2) {
					break;
				}
		    	i++;
		    }
		}
		
		public void clickOnDeleteButton() {
			wdc.waitForPageLoad();
			wdc.waitForExpactedElement_150(driver.findElement(DeleteButton));
			actions.click(driver.findElement(DeleteButton)).build().perform();
			wdc.waitForPageLoad();
			wdc.waitForExpactedElement_150(driver.findElement(AgreeAttention));
			actions.click(driver.findElement(AgreeAttention)).build().perform();
			System.out.println("Deleted");
			wdc.waitForPageLoad();
		}
		


		public void EntityNameUpdate() throws Exception{

			
			System.out.println("Updated"+Constant.EditRolename);
			WebElement entityName=driver.findElement(Clickon_Enter_rollname);
			wdc.waitForExpactedElement(entityName);
			actions.doubleClick(entityName)
			.sendKeys(Keys.DELETE)
			.sendKeys(Constant.EditRolename)
			.sendKeys(Keys.TAB)
			.build().perform();
			Thread.sleep(5000);
			
			
		}
		
		public void Clickon_Enter_Security_Groupname(String s) throws Exception {
			WebElement EntityType=driver.findElement(Security_GroupName);
			wdc.waitForExpactedElement(EntityType);
			actions.click(EntityType).sendKeys(s).build().perform();		
			System.out.println("Entered Security GroupaName");
			Thread.sleep(10000);
		}
		public void Clickon_right_saveButton() throws Exception {
			WebElement EntityType=driver.findElement(Clickon_right_saveButton);
			wdc.waitForExpactedElement(EntityType);
			actions.click(EntityType).build().perform();		
			System.out.println("clicked on Savebutton");
			Thread.sleep(4000);

         }
		public void  clickAndSelect_Entitytype(String Entitytype) throws Exception {    	
	    	wdc.waitForPageLoad();
			wdc.waitForExpactedElement_60(driver.findElement(clickOn_Entitytype));
	        WebElement clickOn_Entitytype1 =driver.findElement(clickOn_Entitytype);
	        actions.click(clickOn_Entitytype1).sendKeys(Entitytype).sendKeys(Keys.ENTER).build().perform();	               
	 		Thread.sleep(5000);
	 		System.out.println("Selected the Entity Type");        
         }
		public void verifyCreatedSecurityGroup() {
	    	wdc.waitForPageLoad();
			wdc.waitForExpactedElement_60(driver.findElement(Security_GroupName));
			try {
				if(driver.findElement(Security_GroupName).isDisplayed()) {
					System.out.println("Created Security Name is Displaying");
				}
			} catch (Exception e) {
				System.out.println("Created Security Name is Not Displaying");
			}

		}
		public void updateSecurityGroupName(String s) throws Exception {
			wdc.waitForPageLoad();
			wdc.waitForExpactedElement_150(driver.findElement(Security_GroupName));
			WebElement groupName = driver.findElement(Security_GroupName);
			actions.doubleClick(groupName).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
			wdc.waitForPageLoad();
			System.out.println("Updated Security Group Name");
		}
		public void clickOnGroupCloseButton() throws Exception {
			wdc.waitForPageLoad();
			wdc.waitForExpactedElement(driver.findElement(ClickonGroupCloseButton));
			actions.click(driver.findElement(ClickonGroupCloseButton)).build().perform();		
			System.out.println("clicked on closebutton");
			Thread.sleep(4000);
		}
	
		
		
}